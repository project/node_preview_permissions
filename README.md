# Node Preview Permissions

Set permissions to use the node save preview.

## Usage

1. Download and install the `drupal/node_preview_permissions` module. Recommended install method is composer:
   ```
   composer require drupal/node_preview_permissions
   ```
2. Go to the "Permissions" configuration page (/admin/people/permissions).
3. Locate the "Node Preview Permissions" section.
4. Check permissions for the targeted roles and save changes.
