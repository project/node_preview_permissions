<?php

namespace Drupal\node_preview_permissions;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\NodeInterface;

/**
 * Determines access to node previews by permissions.
 *
 * @ingroup node_access
 */
class NodePreviewPermissionsAccessCheck implements AccessInterface {

  /**
   * Checks node preview access using permissions.
   *
   * Used in conjunction with the access_check.node.preview provider.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user.
   * @param \Drupal\node\NodeInterface $node_preview
   *   The node that is being previewed.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   *
   * @see \Drupal\node\Access\NodePreviewAccessCheck::access()
   */
  public function access(AccountInterface $account, NodeInterface $node_preview): AccessResultInterface {
    return AccessResult::allowedIfHasPermissions(
      $account,
      ['use node preview', "use {$node_preview->bundle()} node preview"],
      'OR'
    );
  }

}
