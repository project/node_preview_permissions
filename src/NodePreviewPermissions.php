<?php

namespace Drupal\node_preview_permissions;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides node preview permissions.
 */
class NodePreviewPermissions implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * NodePreviewPermissions constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('entity_type.manager'));
  }

  /**
   * Define node preview permissions for each content type.
   *
   * @return array
   *   Returns an array of permissions.
   */
  public function permissions(): array {
    $permissions = [];
    foreach ($this->entityTypeManager->getStorage('node_type')->loadMultiple() as $entityType) {
      /** @var \Drupal\node\NodeTypeInterface $entityType */
      $permissions["use {$entityType->id()} node preview"] = [
        'title' => $this->t('Use node preview on @bundle content', [
          '@bundle' => $entityType->label(),
        ]),
      ];
    }
    return $permissions;
  }

}
